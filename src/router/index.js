import Vue from 'vue'
import Router from 'vue-router'
import Landingpage from '@/components/Landingpage'
import Floorplanspage from '@/components/Floorplanspage'
import Explorepage from '@/components/Explorepage'

Vue.use(Router)

export default new Router({
  /*mode: 'history',*/
  linkActiveClass: 'active',
  routes: [
    {
      path: '/',
      name: 'Landing',
      component: Landingpage
    },
    {
      path: '/floorplans',
      name: 'Floorplans',
      component: Floorplanspage
    },
    {
      path: '/explore',
      name: 'explore',
      component: Explorepage
    }
  ]
})
