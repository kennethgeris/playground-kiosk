// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import BootstrapVue from 'bootstrap-vue'
import IdleVue from 'idle-vue'

const eventsHub = new Vue();
Vue.config.productionTip = false;
Vue.use(BootstrapVue);
Vue.use(IdleVue, {
  eventEmitter: eventsHub,
  idleTime: 2400000
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
  onIdle() {
    console.log(this.$router.push("/"));
  },
})
